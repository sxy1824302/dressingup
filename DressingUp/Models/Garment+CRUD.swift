//
//  Garment+CRUD.swift
//  DressingUp
//
//  Created by alexander sun on 5/19/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import Foundation
import CoreData


extension Garment{
    
    func filePath() -> String {
        if let name = self.fileName{
            return ImagesManager.sharedImagesManager.imageCachePath+"/\(name)"
        }
        return ""
    }
    
    static func dressWithPictureName(_ pictureName:String) -> Garment {
        let entity =  NSEntityDescription.entity(forEntityName: "Garment", in: CoreDataManager.sharedCoreDataManager.managedObjectContext)!
        let dress =  Garment(entity: entity, insertInto: CoreDataManager.sharedCoreDataManager.managedObjectContext)
        
        dress.fileName = pictureName
        
        dress.createdDate = Date() as NSDate?
        
        return dress
    }
    
    @discardableResult static func dressByMappingValues(_ values:Dictionary<String,AnyObject>) -> Garment {
        let entity =  NSEntityDescription.entity(forEntityName: "Garment", in: CoreDataManager.sharedCoreDataManager.managedObjectContext)!
        let dress =  Garment(entity: entity, insertInto: CoreDataManager.sharedCoreDataManager.managedObjectContext)
        
        for (key,value) in values{
            dress.setValue(value, forKey: key)
        }
        

        
        return dress
    }
}
