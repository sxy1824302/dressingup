//
//  Design+CoreDataProperties.swift
//  
//
//  Created by alexander sun on 10/10/2016.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData
 

extension Design {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Design> {
        return NSFetchRequest<Design>(entityName: "Design");
    }

    @NSManaged public var canvasSize: NSObject?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var thumbnail: String?
    @NSManaged public var designGarments: NSSet?

}

// MARK: Generated accessors for designGarments
extension Design {

    @objc(addDesignGarmentsObject:)
    @NSManaged public func addToDesignGarments(_ value: DesignGarment)

    @objc(removeDesignGarmentsObject:)
    @NSManaged public func removeFromDesignGarments(_ value: DesignGarment)

    @objc(addDesignGarments:)
    @NSManaged public func addToDesignGarments(_ values: NSSet)

    @objc(removeDesignGarments:)
    @NSManaged public func removeFromDesignGarments(_ values: NSSet)

}
