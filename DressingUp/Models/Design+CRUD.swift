//
//  Design+CRUD.swift
//  DressingUp
//
//  Created by alexander sun on 5/24/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import CoreData

extension Design{
    static func design(_ canvasSize:CGSize) -> Design {
        let entity =  NSEntityDescription.entity(forEntityName: "Design", in: CoreDataManager.sharedCoreDataManager.managedObjectContext)!
        let design =  Design(entity: entity, insertInto: CoreDataManager.sharedCoreDataManager.managedObjectContext)
        design.canvasSize = NSValue(cgSize: canvasSize)

       
        return design
    }
    
    func filePath() -> String {
        if let name = self.thumbnail{
            return ImagesManager.sharedImagesManager.thumbnailCachePath+"/\(name)"
        }
        return ""
    }
    override public func awakeFromInsert() {
        super.awakeFromInsert()
        self.createdDate = Date() as NSDate?
    }
    
    
    func saveThumbnail(_ name:String) {
        
        if let old = self.thumbnail{
            let path = ImagesManager.sharedImagesManager.thumbnailCachePath+old
            
            do {
                try FileManager.default.removeItem(atPath: path)
            }catch{}
        }
        
        self.thumbnail = name
        
    }
    
}
