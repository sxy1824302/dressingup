//
//  DesignGarment+CRUD.swift
//  DressingUp
//
//  Created by alexander sun on 5/25/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import CoreData

extension DesignGarment{
    static func designGarment(_ garment:Garment,design:Design) -> DesignGarment {
        let entity =  NSEntityDescription.entity(forEntityName: "DesignGarment", in: CoreDataManager.sharedCoreDataManager.managedObjectContext)!
        let dg =  DesignGarment(entity: entity, insertInto: CoreDataManager.sharedCoreDataManager.managedObjectContext)
        dg.design = design
        dg.garment = garment
        
        
        return dg
    }
    
    
    var frameValue:CGRect{
        set(newFrame){
            frame = NSValue(cgRect: newFrame)
        }
        get{
            let v = self.frame as! NSValue
            return v.cgRectValue
        }
    }
    
    
}
