//
//  DesignGarment+CoreDataProperties.swift
//  
//
//  Created by alexander sun on 10/10/2016.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension DesignGarment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DesignGarment> {
        return NSFetchRequest<DesignGarment>(entityName: "DesignGarment");
    }

    @NSManaged public var frame: NSObject?
    @NSManaged public var z: NSNumber?
    @NSManaged public var design: Design?
    @NSManaged public var garment: Garment?

}
