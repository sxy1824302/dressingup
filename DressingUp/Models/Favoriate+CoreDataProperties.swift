//
//  Favoriate+CoreDataProperties.swift
//  
//
//  Created by alexander sun on 10/10/2016.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Favoriate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favoriate> {
        return NSFetchRequest<Favoriate>(entityName: "Favoriate");
    }

    @NSManaged public var createdDate: NSDate?
    @NSManaged public var garment: Garment?

}
