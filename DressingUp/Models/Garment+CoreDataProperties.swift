//
//  Garment+CoreDataProperties.swift
//  
//
//  Created by alexander sun on 10/10/2016.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Garment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Garment> {
        return NSFetchRequest<Garment>(entityName: "Garment");
    }

    @NSManaged public var cost: NSNumber?
    @NSManaged public var costLocale: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var fileName: String?
    @NSManaged public var garmentType: NSNumber?
    @NSManaged public var name: String?
    @NSManaged public var note: String?
    @NSManaged public var purchaseDate: NSDate?
    @NSManaged public var season: NSNumber?
    @NSManaged public var designGarments: NSSet?
    @NSManaged public var favorite: Favoriate?

}

// MARK: Generated accessors for designGarments
extension Garment {

    @objc(addDesignGarmentsObject:)
    @NSManaged public func addToDesignGarments(_ value: DesignGarment)

    @objc(removeDesignGarmentsObject:)
    @NSManaged public func removeFromDesignGarments(_ value: DesignGarment)

    @objc(addDesignGarments:)
    @NSManaged public func addToDesignGarments(_ values: NSSet)

    @objc(removeDesignGarments:)
    @NSManaged public func removeFromDesignGarments(_ values: NSSet)

}
