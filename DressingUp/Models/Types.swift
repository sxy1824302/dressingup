//
//  GarmentType.swift
//  DressingUp
//
//  Created by alexander sun on 5/27/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import Foundation
enum GarmentType:Int {
    case tops = 1
    case pants,shoes,others
    
    func  description() -> String {
        switch self {
        case .tops:
            return "Tops"
        case .pants:
            return "Pants"
        case .shoes:
            return "Shoes"
        case .others:
            return "Others"

        }
    }
}


enum Season:Int {
    case spring = 0x0001
    case summer = 0x0010
    case fall = 0x0100
    case winter = 0x1000
}

