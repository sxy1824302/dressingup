//
//  DesignCollectionViewCell.swift
//  DressingUp
//
//  Created by alexander sun on 5/24/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import Material
class DesignCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var selectionEffectView: UIView!
    @IBOutlet weak var checkIconView: UIImageView!
    
    var thumbnail: UIImage? {
        didSet {
            thumbnailView.image = thumbnail
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionEffectView.isHidden = true
        checkIconView.image = Icon.check
        contentView.backgroundColor = UIColor.clear
    }
    
    func configCell(_ design:Design) -> Void {
        
        let image = UIImage(contentsOfFile: design.filePath())
        
        thumbnail = image
        
        
    }
    override var isSelected: Bool{
        didSet{
            selectionEffectView.isHidden = !isSelected
        }
    }
    
    override func prepareForReuse() {

        thumbnail = nil
    }
}
