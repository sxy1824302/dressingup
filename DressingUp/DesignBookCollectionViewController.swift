//
//  DesignBookCollectionViewController.swift
//  DressingUp
//
//  Created by alexander sun on 5/19/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import CoreData
import Crashlytics
private let reuseIdentifier = "DesignCollectionViewCell"

private let ShareDesignEvent = "ShareDesignEvent"
class DesignBookCollectionViewController: UICollectionViewController {
    @IBOutlet weak var selectButton: UIBarButtonItem!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!

    fileprivate var selectedDesign:Design?
    fileprivate var selectedEditingDesigns = [Design]()

    var fetchedResultsController:NSFetchedResultsController<Design>!
    var selectedDesigns = [String]()
    var editingMode = false{
        didSet{
            if editingMode{
                selectedEditingDesigns.removeAll()
                
                selectButton.title = NSLocalizedString("Cancel", comment: "")
                collectionView?.allowsMultipleSelection = true
                self.tabBarController?.tabBar.isHidden = true
                
                view.addSubview(toolBar)
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[toolBar]-0-|", options: .alignAllCenterX, metrics: nil, views: ["toolBar":toolBar]))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[toolBar]-0-|", options: .alignAllBottom, metrics: nil, views: ["toolBar":toolBar]))
                
            }else{
                collectionView?.indexPathsForSelectedItems?.forEach({ (indexPath) in
                    collectionView?.deselectItem(at: indexPath, animated: false)
                })
                
                selectButton.title = NSLocalizedString("Select", comment: "")
                collectionView?.allowsMultipleSelection = false
                self.tabBarController?.tabBar.isHidden = false
                
                toolBar.removeFromSuperview()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        toolBar.tintColor = UIConfig.DefaultTintColor
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        updateToolBar()
        self.clearsSelectionOnViewWillAppear = true
        collectionView?.emptyDataSetSource = self
        collectionView?.emptyDataSetDelegate = self
       

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if editingMode{
            editingMode = false
        }
        
    }
    func updateToolBar() {
        let enable = selectedEditingDesigns.count > 0
        shareButton.isEnabled = enable
        deleteButton.isEnabled = enable
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let request = NSFetchRequest<Design>(entityName: "Design")
        let descriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        request.sortDescriptors = [descriptor]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreDataManager.sharedCoreDataManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do{
            try fetchedResultsController.performFetch()
        }catch let error{
            print(error)
        }
    }
    func updateSelectButton() {
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .vertical
            layout.sectionInset = UIEdgeInsetsMake(6, 12, 0, 12)
            layout.minimumInteritemSpacing = 12
            layout.minimumLineSpacing = 1
            
            
            let basicSide = min(view.frame.width, view.frame.height) - 12*2
            
            
            var smallSide:CGFloat = basicSide
            
            
            var itemPerRow:CGFloat
            
            repeat{
                itemPerRow = floor(smallSide/100)
                
                smallSide = basicSide - (itemPerRow-1)*layout.minimumInteritemSpacing
                
            }while(floor(smallSide/100) != itemPerRow)
            
            
            
            if (smallSide.truncatingRemainder(dividingBy: 100)>50){
                
                itemPerRow += 1
                
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth*1.7777)
            }else{
                
                
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth*1.7777)
            }
            
            
            
            
        }
        
    }

    @IBAction func selectDesignBooks(_ sender: AnyObject) {
        
        editingMode = !editingMode
    

    }


    @IBAction func addDesign(_ sender: AnyObject) {
        
        performSegue(withIdentifier: "GotoDesignSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let design = selectedDesign{
            let dv = segue.destination as! DesignViewController
            dv.design = design
            selectedDesign = nil
        }
     
    }
    

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return fetchedResultsController.sections?.count ?? 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! DesignCollectionViewCell
        
        let design = fetchedResultsController.object(at: indexPath)
        
        cell.configCell(design)
        return cell
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let design = fetchedResultsController.object(at: indexPath)
        
   
        if !editingMode{
            collectionView.deselectItem(at: indexPath, animated: true)
            
       
            selectedDesign = design
            
             performSegue(withIdentifier: "GotoDesignSegue", sender: self)
        }else{
            selectedEditingDesigns.append(design)
            updateToolBar()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let design = fetchedResultsController.object(at: indexPath)
        if let idx = selectedEditingDesigns.index(of: design){
            selectedEditingDesigns.remove(at: idx)
          
        }
        updateToolBar()
        
        
    }
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}

extension DesignBookCollectionViewController:NSFetchedResultsControllerDelegate{
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView?.reloadData()
    }
}

extension DesignBookCollectionViewController{
    
    @IBAction func share(_ sender: AnyObject) {
        
        let item = sender as! UIBarButtonItem
        let images = Array(selectedEditingDesigns.flatMap{ UIImage(contentsOfFile: $0.filePath())})
        
        let vc = UIActivityViewController(activityItems: images, applicationActivities: nil)
        
        vc.popoverPresentationController?.barButtonItem = item
            
        present(vc, animated: true, completion: nil)
        
        Answers.logCustomEvent(withName: ShareDesignEvent, customAttributes: nil)
    }
    @IBAction func deleteSelected(_ sender: AnyObject) {
        let alert = UIAlertController(title: nil, message: NSLocalizedString("Are you sure to delete selected items?", comment: ""), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (action) in
            self.selectedEditingDesigns.forEach({ (design) in
                CoreDataManager.sharedCoreDataManager.managedObjectContext.delete(design)
            })
            self.collectionView?.indexPathsForSelectedItems?.forEach({ (indexPath) in
                self.collectionView?.deselectItem(at: indexPath, animated: true)
            })
            CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
            
            self.selectedEditingDesigns.removeAll()
            
            self.updateToolBar()
            
        }))
        
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler:nil) )
        
        present(alert, animated: true, completion: nil)

    }
}

extension DesignBookCollectionViewController:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: NSLocalizedString("Start to design your own style", comment: ""), attributes: [NSForegroundColorAttributeName:UIConfig.DefaultTintColor])
    }
    func buttonImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return UIImage(named: "button-pencil")
    }
    
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        performSegue(withIdentifier: "GotoDesignSegue", sender: self)
    }
}
