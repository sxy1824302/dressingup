//
//  UIConfig.swift
//  DressingUp
//
//  Created by alexander sun on 5/30/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

struct UIConfig {
    static let DefaultTintColor = UIColor(hex6: 0xdb639b)
}