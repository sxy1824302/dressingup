//
//  ClosetViewController.swift
//  DressingUp
//
//  Created by alexander sun on 5/19/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import ImagePicker
import Material
import Crashlytics

enum FilterType:Int {
    case tops = 0
    case pants,shoes,others
}

private let ShareGarmentEvent = "ShareGarmentEvent"
private let CreateGarmentEvent = "CreateGarment"
private let CreateGarmentEventType = "Type"
class ClosetViewController: UIViewController {
    var selectedGarments = [Garment]()
    var selectedGarment:Garment?
    @IBOutlet weak var selectButton: UIBarButtonItem!
    var editingMode = false{
        didSet{
            
            if editingMode{
                
                selectedGarments.removeAll()
                
                bottomLayoutConstraint.constant = -5
                selectButton.title = NSLocalizedString("Cancel", comment: "")
                collectionviewController.collectionView?.allowsMultipleSelection = true
                self.tabBarController?.tabBar.isHidden = true
                
                view.addSubview(toolBar)
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[toolBar]-0-|", options: .alignAllCenterX, metrics: nil, views: ["toolBar":toolBar]))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[toolBar]-0-|", options: .alignAllBottom, metrics: nil, views: ["toolBar":toolBar]))
                
            }else{
                bottomLayoutConstraint.constant = 0
                collectionviewController.collectionView?.indexPathsForSelectedItems?.forEach({ (indexPath) in
                    collectionviewController.collectionView?.deselectItem(at: indexPath, animated: false)
                })
                
                selectButton.title = NSLocalizedString("Select", comment: "")
                collectionviewController.collectionView?.allowsMultipleSelection = false
                self.tabBarController?.tabBar.isHidden = false
                
                toolBar.removeFromSuperview()
            }
            
            collectionviewController.inEditingMode = editingMode
        }
    }
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareButton: UIBarButtonItem!
    
    @IBOutlet weak var cameraButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var selectedPhotoType:GarmentType = .tops
    
    weak var collectionviewController:ClosetCollectionViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.tintColor = UIConfig.DefaultTintColor
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        updateToolBar()
        

    }
    @IBAction func didSelectClothType(_ sender: AnyObject) {
        
        if let segment = sender as? UISegmentedControl{
            switch FilterType(rawValue: segment.selectedSegmentIndex)  {
            case .some(.tops):
                collectionviewController.reloadGarmentData(NSPredicate(format: "garmentType == %lli",GarmentType.tops.rawValue))
            case .some(.pants):
                collectionviewController.reloadGarmentData(NSPredicate(format: "garmentType == %lli",GarmentType.pants.rawValue))
            case .some(.shoes):
                collectionviewController.reloadGarmentData(NSPredicate(format: "garmentType == \(GarmentType.shoes.rawValue)"))
            case .some(.others):
                collectionviewController.reloadGarmentData(NSPredicate(format: "garmentType == %lli",GarmentType.others.rawValue))
            default:
                break
            }
        }
        
        
    }
    
    @IBAction func selectGarments(_ sender: AnyObject) {
        
        editingMode = !editingMode

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if editingMode{
            editingMode = false
        }
        
    }
    
    // MARK: selection
    func didSelectGarment(_ garment:Garment) {
        
        if editingMode {
            selectedGarments.append(garment)
            
            updateToolBar()
        }else{
            selectedGarment = garment
            performSegue(withIdentifier: "DetailSegue", sender: nil)
        }

    }
    
    func didDeselectGarment(_ garment:Garment) {
        if editingMode {
            if let idx = selectedGarments.index(of: garment){
                selectedGarments.remove(at: idx)
            }
            updateToolBar()
        }

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Embed"{
            collectionviewController = segue.destination as! ClosetCollectionViewController
            collectionviewController.closetViewController = self
        }else if segue.identifier == "DetailSegue"{
            let detailVC = segue.destination as! GarmentDetailTableViewController
            detailVC.garment = selectedGarment!
        }
    }
    
    
    
    func createGarment(_ image:UIImage) {
        if let name = ImagesManager.sharedImagesManager.saveImage(image){
            let garment = Garment.dressWithPictureName(name)
            garment.garmentType = NSNumber(value: self.selectedPhotoType.rawValue as Int)
        }
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
        
        
        Answers.logCustomEvent(withName: CreateGarmentEvent, customAttributes: [CreateGarmentEventType:self.selectedPhotoType.description()])
    }
    
}
extension ClosetViewController:ImagePickerDelegate{
    
    
    
    @IBAction func addPhoto(_ sender: AnyObject) {
        
        let button = sender as? UIBarButtonItem
        
        
        let actions = UIAlertController(title: "", message: NSLocalizedString("Select a type", comment: ""), preferredStyle: .actionSheet)
        
        actions.popoverPresentationController?.barButtonItem = button
            
        actions.addAction(UIAlertAction(title: NSLocalizedString("Tops", comment: ""), style: .default, handler: { (action) in
            self.selectedPhotoType = .tops
            self.showCamera()
        }))
        actions.addAction(UIAlertAction(title: NSLocalizedString("Pants", comment: ""), style: .default, handler: { (action) in
            self.selectedPhotoType = .pants
            self.showCamera()
        }))
        actions.addAction(UIAlertAction(title: NSLocalizedString("Shoes", comment: ""), style: .default, handler: { (action) in
            self.selectedPhotoType = .shoes
            self.showCamera()
        }))
        actions.addAction(UIAlertAction(title: NSLocalizedString("Others", comment: ""), style: .default, handler: { (action) in
            self.selectedPhotoType = .others
            self.showCamera()
        }))
        actions.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler:nil))
        
        present(actions, animated: true, completion: nil)

        
    }
    
    func showCamera() {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 6
        present(imagePickerController, animated: true, completion: nil)
    }
    func showCameraFromEmpty() {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.selectedPhotoType = .tops
        case 1:
            self.selectedPhotoType = .pants
        case 2:
            self.selectedPhotoType = .shoes
        case 3:
            self.selectedPhotoType = .others
        default:
            break
        }
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 6
        present(imagePickerController, animated: true, completion: nil)
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        images.forEach { (image) in
             createGarment(image)
        }
        
        dismiss(animated: true, completion: nil)
       
    }

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }

    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
         dismiss(animated: true, completion: nil)
    }

}

extension ClosetViewController{
    

    func updateToolBar() {
        let enable = selectedGarments.count > 0
        shareButton.isEnabled = enable
        deleteButton.isEnabled = enable
    }
    
    @IBAction func share(_ sender: AnyObject) {
        
        let item = sender as! UIBarButtonItem
  
        
        let images = Array(selectedGarments.flatMap{ UIImage(contentsOfFile: $0.filePath())})
        
        let vc = UIActivityViewController(activityItems: images, applicationActivities: nil)
        vc.popoverPresentationController?.barButtonItem = item
        
        present(vc, animated: true, completion: nil)
        
        Answers.logCustomEvent(withName: ShareGarmentEvent, customAttributes: nil)
    }
    
    @IBAction func deleteSelected(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: nil, message: NSLocalizedString("Are you sure to delete selected items?", comment: ""), preferredStyle: .alert)
  
        alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (action) in
            self.selectedGarments.forEach({ (garment) in
                CoreDataManager.sharedCoreDataManager.managedObjectContext.delete(garment)
            })
            self.collectionviewController.collectionView?.indexPathsForSelectedItems?.forEach({ (indexPath) in
                self.collectionviewController.collectionView?.deselectItem(at: indexPath, animated: true)
            })
            CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
            
            self.selectedGarments.removeAll()
            
            self.updateToolBar()

        }))
        
 
        alert.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel, handler:nil) )
        
        present(alert, animated: true, completion: nil)
    }

}





