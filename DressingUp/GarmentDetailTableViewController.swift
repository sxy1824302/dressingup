//
//  GarmentDetailTableViewController.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

enum InputDataType {
    case decimal,date,longText,shortText
    
    
    var editViewControllerIdentifier:String?{
        switch self {
        case .decimal:
            return "DecimalEditViewController"
        case .date:
            return "DateEditViewController"
        case .longText:
            return "TextViewViewController"
        case .shortText:
            return "ShortTextEditViewController"
        }
    }
    
}

class GarmentDetailTableViewController: UITableViewController {
    
    let locale = Locale.current
    
    var currentDate:Date = Date()
    
    lazy var dateFormatter:DateFormatter = {
        let f = DateFormatter()
        f.dateStyle = .medium
        return f
    }()
    
    @IBOutlet weak var imgageProcessButton: UIBarButtonItem!
    
    var garment:Garment!
    
    @IBAction func goToProcessImage(_ sender: AnyObject) {
        performSegue(withIdentifier: "EnterImageProcessing", sender: self)
    }
    
    enum CellConfiguration {
        case nameCell,imageCell,purchaseDateCell,costCell,noteCell
        
        
        
        var cellIdentifier:String{
            switch self {
            case .nameCell:
                return "GarmentSimpleDescriptionTableViewCell"
            case .imageCell:
                return "GarmentImageTableViewCell"
            case .purchaseDateCell,.costCell:
                return "GarmentSimpleDescriptionTableViewCell"
            case .noteCell:
                return "GarmentTextTableViewCell"
            }
        }
        
        var dataType:InputDataType{
            switch self {
            case .nameCell:
                return .shortText
            case .costCell:
                return .decimal
            case .purchaseDateCell:
                return .date
            case .noteCell:
                return .longText
            default:
                return .shortText
            }
        }
        
        
        var cellTitle:String?{
            switch self {
            case .purchaseDateCell:
                  return NSLocalizedString("Bought On", comment: "")
            case .costCell:
                return NSLocalizedString("Cost", comment: "")
            case .noteCell:
                return NSLocalizedString("Note", comment: "")
            case .nameCell:
                return NSLocalizedString("Name", comment: "")
            default:
                return nil
            }
        }
        

        
        var cellHeight:CGFloat{
            switch self {
            case .imageCell:
                return 180
            case .purchaseDateCell,.costCell,.nameCell:
                return 48
            case .noteCell:
                return UITableViewAutomaticDimension
            }
        }
        
        
        var editable:Bool{
            switch self {
            case .purchaseDateCell,.costCell,.noteCell,.nameCell:
                return true
            default :
                return false
            }
        }
        
    }
    
    let cells:[CellConfiguration] = [.imageCell,.nameCell,.costCell,.purchaseDateCell,.noteCell]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }


    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return cells.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        let item = cells[(indexPath as NSIndexPath).row]
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cellIdentifier, for: indexPath)
        
        switch item {
        case .imageCell:
            let c  = cell as! GarmentImageTableViewCell
            let image = UIImage(contentsOfFile: garment.filePath())
            c.garmentImageView?.image = image
        case .nameCell:
            let c  = cell as! GarmentSimpleDescriptionTableViewCell
            
            if let name = garment.name{
                c.valueLabel.text = name
            }else{
                c.valueLabel.text = NSLocalizedString("Name your item", comment: "")
            }
            c.titleLabel.text = item.cellTitle
        case .costCell:
            let c  = cell as! GarmentSimpleDescriptionTableViewCell
            if let cost = garment.cost?.floatValue{
                let currency = (locale as NSLocale).object(forKey: NSLocale.Key.currencySymbol) as? String ?? ""
                c.valueLabel.text = "\(cost) \(currency)"
            }
            c.titleLabel.text = item.cellTitle
        case .purchaseDateCell:
            let c  = cell as! GarmentSimpleDescriptionTableViewCell
            if let date = garment.purchaseDate{
                c.valueLabel.text = dateFormatter.string(from: date as Date)
            }else{
                c.valueLabel.text = NSLocalizedString("Tap to choose", comment: "")
            }
            c.titleLabel.text = item.cellTitle
        case .noteCell:
            let c  = cell as! GarmentTextTableViewCell
            c.valueLabel.text = garment.note ?? NSLocalizedString("Tap to edit", comment: "")
            c.titleLabel.text = item.cellTitle
        }
        
        cell.accessoryType = item.editable ? .disclosureIndicator : .none
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = cells[(indexPath as NSIndexPath).row]
        return item.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = cells[(indexPath as NSIndexPath).row]
        return item.cellHeight
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = cells[(indexPath as NSIndexPath).row]
        guard  let sid = item.dataType.editViewControllerIdentifier,let evc = storyboard?.instantiateViewController(withIdentifier: sid) as? EditTarget else {
            return
        }
        
        
        switch item {
        case .nameCell:
            evc.initiateValue = self.garment.name as AnyObject?
            
            evc.applyEditing = {value in
                let date = value as! String
                self.garment.name = date
            }
        case .costCell:
            evc.initiateValue = self.garment.cost
            
            evc.applyEditing = {value in
                let cost = value as! Int
                self.garment.cost = cost as NSNumber?
                self.garment.costLocale = self.locale.identifier
            }
        case .purchaseDateCell:
            evc.initiateValue = self.garment.purchaseDate
            
            evc.applyEditing = {value in
                let date = value as! Date
                self.garment.purchaseDate = date as NSDate?
            }
        case .noteCell:
            evc.initiateValue = self.garment.note as AnyObject?
            
            evc.applyEditing = {value in
                let note = value as! String
                self.garment.note = note
            }
        default:
            return
        }
      
        navigationController?.pushViewController(evc as! UIViewController, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let fvc = segue.destination as! ImageProcessingViewController
        fvc.targetImage = UIImage(contentsOfFile: garment.filePath())!
    }
    

}
