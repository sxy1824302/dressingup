//
//  EditTarget.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

protocol EditTarget:class {

    var applyEditing:((AnyObject?)->(Void))? {get set}
    
    var initiateValue:AnyObject?  {get set}
}
