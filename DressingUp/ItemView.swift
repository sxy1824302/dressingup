//
//  ItemView.swift
//  DressingUp
//
//  Created by alexander sun on 5/18/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

let DesignzOrderChangedNotification =  "DesignzOrderChangedNotification"
let ChangeAccessoryLayerNotificationShowKey = "ChangeAccessoryLayerNotificationShowKey"
let ChangeAccessoryLayerNotification =  "ChangeAccessoryLayerNotification"


let ItemStartedMoveNotification =  "ItemStartedMoveNotification"
let ItemEndedMoveNotification =  "ItemEndedMoveNotification"

class ItemView: UIView {
    
    weak var trashView:UIImageView?
    
    var designGarment:DesignGarment
    var imageView:UIImageView!
    
    var isIntrash = false
    
    var initialLocation: CGPoint = CGPoint.zero
    
    var scaleFactor:CGFloat = 1
    
    var panGesture:UIPanGestureRecognizer!
    
    var pinchGesture:UIPinchGestureRecognizer!
    
    let dashedBoarder = CAShapeLayer()
    
    var ob:AnyObject!,ob3:AnyObject!
    
    init(frame: CGRect,garment:Garment,design:Design) {
        
        designGarment = DesignGarment.designGarment(garment, design: design)
        designGarment.frameValue = frame
        super.init(frame: frame)

        setup()
    }
    
    deinit{
         NotificationCenter.default.removeObserver(ob)
        NotificationCenter.default.removeObserver(ob3)
    }
    func setup() {
        
        imageView = UIImageView(frame: frame)
        imageView.frame = self.bounds
        imageView.contentMode = .scaleAspectFit
        addSubview(imageView)
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 10))
        path.addLine(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 10, y: 0))

        
        path.move(to: CGPoint(x: self.bounds.size.width-10,y: 0))
        path.addLine(to: CGPoint(x: self.bounds.size.width, y: 0))
        path.addLine(to: CGPoint(x: self.bounds.size.width, y: 10))
        
        path.move(to: CGPoint(x: self.bounds.size.width,y: self.bounds.size.height-10))
        path.addLine(to: CGPoint(x: self.bounds.size.width, y: self.bounds.size.height))
        path.addLine(to: CGPoint(x: self.bounds.size.width-10, y: self.bounds.size.height))
        
        path.move(to: CGPoint(x: 10,y: self.bounds.size.height))
        path.addLine(to: CGPoint(x: 0, y: self.bounds.size.height))
        path.addLine(to: CGPoint(x: 0, y: self.bounds.size.height-10))
        
        
        dashedBoarder.fillColor = nil
        dashedBoarder.strokeColor = UIColor.gray.cgColor
        dashedBoarder.lineWidth = 4
        dashedBoarder.lineJoin = kCALineJoinRound
        dashedBoarder.lineCap = kCALineCapRound
   
        dashedBoarder.path = path.cgPath
        layer.addSublayer(dashedBoarder)
        dashedBoarder.isHidden = true
        
        weak var weakSelf = self
        
        ob = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ChangeAccessoryLayerNotification), object: nil, queue: OperationQueue.main) { (noti) in
            if let show =  (noti as NSNotification).userInfo?[ChangeAccessoryLayerNotificationShowKey] as? Bool,let garment = noti.object as? DesignGarment , garment  == weakSelf!.designGarment{
                weakSelf?.showEditingLayer(show)
            }else{
                weakSelf?.showEditingLayer(false)
            }
        }
        
        ob3 = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: DesignzOrderChangedNotification), object: nil, queue: OperationQueue.main) { (noti) in
            
            if let garment = noti.object as? DesignGarment{
                weakSelf?.adjustZOrder(garment)
            }
        }
        
    }
    
    func adjustZOrder(_ garment:DesignGarment) {
        
        if  garment  == self.designGarment{
            self.designGarment.z = NSNumber(value: self.superview!.subviews.index(of: self)!)
        }else{
            self.designGarment.z = NSNumber(value: max( self.designGarment.z!.intValue - 1, 0) as Int)
        }

    }
    
    init(aDesignGarment:DesignGarment) {
        
        designGarment = aDesignGarment
        
        super.init(frame: designGarment.frameValue)
        
         setup()
    }
    
    func showEditingLayer(_ show:Bool)  {
        if show{
            dashedBoarder.isHidden = false
        }else{
            dashedBoarder.isHidden = true
           
        }
    }

    
    override func didMoveToSuperview() {
        if self.superview != nil {
            self.registerDraggability()
        } else {
            self.removeDraggability()
        }
    }
    func registerDraggability() {
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(ItemView.didPan))
 
        self.addGestureRecognizer(panGesture)

        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(ItemView.didPinch(_:)))
        
        self.addGestureRecognizer(pinchGesture)

    }
    
    func removeDraggability() {
        guard self.gestureRecognizers != nil else {
            return
        }
        removeGestureRecognizer(panGesture)
        removeGestureRecognizer(pinchGesture)
        panGesture = nil
        pinchGesture = nil
    }
    
    func didPinch(_ pinchGesture:UIPinchGestureRecognizer) {
        
        switch pinchGesture.state {
        case UIGestureRecognizerState.began:
            startInteracting(self.center)
        case UIGestureRecognizerState.ended:
            scaleFactor = scaleFactor*pinchGesture.scale
            designGarment.frameValue = frame
        case UIGestureRecognizerState.failed, UIGestureRecognizerState.cancelled:
            self.transform = CGAffineTransform(scaleX: scaleFactor,y: scaleFactor)
        default:
            let sf  = scaleFactor*pinchGesture.scale
            
            if sf >= 0.2 && sf < 2{
                  self.transform = CGAffineTransform(scaleX: sf,y: sf)
            }
          
            break
        }
 
        
    }
    
    func startInteracting(_ initialLocation:CGPoint)  {
        self.initialLocation = initialLocation
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAccessoryLayerNotification), object: designGarment, userInfo: [ChangeAccessoryLayerNotificationShowKey:true])
    }
    
    func didPan(_ panGesture: UIPanGestureRecognizer) {
        
        checkTrashView()
        
        switch panGesture.state {
        case UIGestureRecognizerState.began:
            startInteracting(self.center)
            if superview!.subviews.index(of: self) != superview!.subviews.count-1 {
                superview?.bringSubview(toFront: self)
                NotificationCenter.default.post(name: Notification.Name(rawValue: DesignzOrderChangedNotification), object: designGarment, userInfo: nil)
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: ItemStartedMoveNotification), object: self, userInfo:nil)
        case UIGestureRecognizerState.ended,UIGestureRecognizerState.failed,UIGestureRecognizerState.cancelled:
            designGarment.frameValue = frame
            NotificationCenter.default.post(name: Notification.Name(rawValue: ItemEndedMoveNotification), object: self, userInfo:nil)
            
            if isIntrash {
                removeFromSuperview()
                designGarment.design = nil
                CoreDataManager.sharedCoreDataManager.managedObjectContext.delete(designGarment)
                CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
            }
            
        default:
            break
        }
        if let spv = self.superview{
            let translation = panGesture.translation(in: spv)
            self.center = CGPoint(x: self.initialLocation.x + translation.x, y: self.initialLocation.y + translation.y)
            
        }

    }
    
    func checkTrashView() {
        if let tv = trashView,let baseView = trashView?.superview{
            
            let itemFrame = self.superview!.convert(frame, to: baseView)
            
            let inTrashBefore =  tv.frame.intersects(itemFrame)
            var newItemFrame:CGRect
            
            let scale = 80/self.frame.width
            
            
            if inTrashBefore {
                let width = (self.frame.width - 80)*0.5
                
                newItemFrame = self.frame.insetBy(dx: width, dy: width)
                
            }else{
                newItemFrame = self.frame
            }
            
            let inTrashAfter =  tv.frame.intersects(newItemFrame)

            if (inTrashBefore&&inTrashAfter) != isIntrash{
               
                    isIntrash = inTrashBefore&&inTrashAfter
  
                    if isIntrash{
                        self.transform = CGAffineTransform(scaleX: scale, y: scale)
                    }else{
                        self.transform = CGAffineTransform(scaleX: scaleFactor,y: scaleFactor)
                    }
             
            }
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



