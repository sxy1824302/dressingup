//
//  ShortTextEditViewController.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class ShortTextEditViewController: UIViewController,EditTarget {
    @IBOutlet weak var textField: UITextField!
    var applyEditing: ((AnyObject?) -> (Void))?
    var initiateValue: AnyObject?
    var lengthLimit = 30
    override func viewDidLoad() {
        super.viewDidLoad()

        
        guard let text = initiateValue as? String else {
            return
        }
        
        textField.text = text
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    @IBAction func done(_ sender: AnyObject) {
        
        guard let text = textField.text,let closure = applyEditing else { return }
        
        if text.utf8.count >= lengthLimit{
            let alert = UIAlertController(title: nil, message: NSLocalizedString("Name is limited to \(lengthLimit) letters", comment: ""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil))
            
            
            present(alert, animated: true, completion: nil)
            return
        }
        
        closure(text as AnyObject?)
        
        navigationController?.popViewController(animated: true)
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
