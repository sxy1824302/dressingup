//
//  ThemeManager.swift
//  DressingUp
//
//  Created by alexander sun on 29/09/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit


class ThemeManager {
    static let sharedThemeManager = ThemeManager()
    
    
    
    init() {
        
    }
    
    
    func loadTheme()  {
        
        UINavigationBar.appearance().tintColor = UIConfig.DefaultTintColor
        
        UITabBar.appearance().tintColor = UIConfig.DefaultTintColor
        
    }
    
}
