//
//  DesignViewController.swift
//  DressingUp
//
//  Created by alexander sun on 5/19/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import Material
import CoreData
import FontAwesome_swift
import Crashlytics
import QuartzCore
let ShouldCloseMenuNotification = "ShouldCloseMenuNotification"

private let StartDesignEvent = "StartDesignEvent"
private let StartDesignEventEnterMethod = "StartDesignEventEnterMethod"
private let StartDesignEventAddGarmentType = "StartDesignEventAddGarmentType"
private let StartDesignEventAddGarmentTypeName = "Type"
class DesignViewController: UIViewController,UIViewControllerTransitioningDelegate,CAAnimationDelegate {
    
    let NotFirstTimeUseKey = "NotFirstTimeUseKey"
    
    @IBOutlet weak var designView: UIView!
    
    var design:Design?

    /// MenuView reference.
    fileprivate lazy var menuView:Menu = Menu()
    
    /// Default spacing size
    let spacing: CGFloat = 16
    
    /// Diameter for FabButtons.
    let diameter: CGFloat = 56
    
    /// Height for FlatButtons.
    let height: CGFloat = 36
    
    let trashView = UIImageView(frame: CGRect(x: 0,y: 0,width: 100,height: 100))
    
    var selectedGarmentType:GarmentType?

    fileprivate var ob1:AnyObject!,ob2:AnyObject!
    
    var firstimeTimer:Timer?
    
    var zoomAnimationLayer = [CAShapeLayer]()
    
    
    deinit{
        NotificationCenter.default.removeObserver(ob1)
        NotificationCenter.default.removeObserver(ob2)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func idleCheck()  {
        
        let ftu = UserDefaults.standard.bool(forKey: NotFirstTimeUseKey)
        
        if !ftu{
            firstimeTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(DesignViewController.showZoomAnimation(_:)), userInfo: nil, repeats: true)
        }
    }
    
    func showZoomAnimation(_ timer:Timer) {
        
        if designView.subviews.count > 0{
            let containerView = designView.subviews.first!
            
            let center = CGPoint(x: containerView.bounds.width*0.5, y: containerView.bounds.height*0.5)
            
            let leftTop = CGPoint(x: 0, y: 0)
            
            let rigthBottom = CGPoint(x: containerView.bounds.width, y: containerView.bounds.height)
            
            let path1 = UIBezierPath()
            
            path1.addArc(withCenter: leftTop, radius: 20, startAngle: 0, endAngle: CGFloat(2*M_PI), clockwise: true)
            
            let layer = CAShapeLayer()
            layer.path = path1.cgPath
            layer.fillColor = UIColor.white.withAlphaComponent(0.4).cgColor
            layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            
            
            let path2 = UIBezierPath()
            
            path2.addArc(withCenter: leftTop, radius: 20, startAngle: 0, endAngle: CGFloat(2*M_PI), clockwise: true)
            
            let layer1 = CAShapeLayer()
            layer1.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            layer1.path = path2.cgPath
            layer1.fillColor = UIColor.white.withAlphaComponent(0.4).cgColor
            
            

            
            let group1 = CAAnimationGroup()
            
            let anima1 = CABasicAnimation(keyPath: "position")
            anima1.fromValue = NSValue(cgPoint: center)
            anima1.toValue = NSValue(cgPoint: leftTop)
            anima1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            
            let solider = CABasicAnimation(keyPath: "fillColor")
            solider.duration = 0.4
            solider.fromValue = UIColor.white.withAlphaComponent(0.4).cgColor
            solider.repeatCount = 0
            solider.fillMode = kCAFillModeForwards
            solider.toValue = UIColor.white.withAlphaComponent(0.9).cgColor
            solider.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            group1.animations = [solider,anima1]
            group1.isRemovedOnCompletion = true
            group1.fillMode = kCAFillModeForwards
            group1.duration = 1.6
            group1.repeatCount = 4//Float.infinity
            group1.delegate = self
            
            let group2 = CAAnimationGroup()
            let anima2 = CABasicAnimation(keyPath: "position")
            anima2.fromValue = NSValue(cgPoint: center)
            anima2.toValue = NSValue(cgPoint: rigthBottom)
            anima2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            
            let solider1 = CABasicAnimation(keyPath: "fillColor")
            solider1.duration = 0.4
            solider1.fromValue = UIColor.white.withAlphaComponent(0.4).cgColor
            solider1.repeatCount = 0
            solider1.fillMode = kCAFillModeForwards
            solider1.toValue = UIColor.white.withAlphaComponent(0.9).cgColor
            solider1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            group2.delegate = self
            group2.animations = [solider1,anima2]
            group2.isRemovedOnCompletion = true
            group2.fillMode = kCAFillModeForwards
            group2.duration = 1.6
            group2.repeatCount = 4//Float.infinity
            
            containerView.layer.addSublayer(layer)
            containerView.layer.addSublayer(layer1)
            
 
            layer.add(group1, forKey: "path_animation")
            layer1.add(group2, forKey: "path_animation")
            
            zoomAnimationLayer.append(layer)
            zoomAnimationLayer.append(layer1)
            
            UserDefaults.standard.set(true, forKey: NotFirstTimeUseKey)
            UserDefaults.standard.synchronize()
            
            firstimeTimer?.invalidate()
            firstimeTimer = nil
        }
        

    }
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        zoomAnimationLayer.forEach { (layer) in
            if layer.animation(forKey: "path_animation") == nil {
                layer.removeFromSuperlayer()
            }

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Answers.logCustomEvent(withName: StartDesignEvent, customAttributes: [StartDesignEventEnterMethod:"viewDidLoad"])
        
        prepareView()
        prepareMenuViewExample()
        
        weak var weakSelf = self
        ob1 = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: SelectedGarmentNotification), object: nil, queue: OperationQueue.main) { (noti) in
            
            if let dress = noti.object as? Garment{
                
                weakSelf?.addGarment(dress)
              
                Answers.logCustomEvent(withName: StartDesignEventAddGarmentType, customAttributes: [StartDesignEventAddGarmentTypeName:GarmentType(rawValue: dress.garmentType!.intValue)!.description()])
            }
            
        }
        
        ob2 = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ShouldCloseMenuNotification), object: nil, queue: OperationQueue.main) { (noti) in
            
            if weakSelf!.menuView.isOpened {
                weakSelf?.menuView.close()
                (weakSelf?.menuView.views.first as? Button)?.animate(animation: Animation.rotate(rotation: 0))
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(DesignViewController.itemStartedMove(_:)), name: NSNotification.Name(rawValue: ItemStartedMoveNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DesignViewController.itemEndedMove(_:)), name: NSNotification.Name(rawValue: ItemEndedMoveNotification), object: nil)


        loadGarmentsIfHave()
        
        
        idleCheck()
    }
    
    func itemStartedMove(_ noti:Notification) {
        if trashView.superview == nil{
            if let iv = noti.object as? ItemView{
                iv.trashView = trashView
            }
            designView.addSubview(trashView)
            designView.sendSubview(toBack: trashView)
            trashView.alpha = 0
            trashView.isHidden = false
            UIView.animate(withDuration: 0.6, animations: {
                self.trashView.alpha = 1
            })
        }
    }
    func itemEndedMove(_ noti:Notification) {
        if trashView.superview != nil{
            trashView.removeFromSuperview()
            trashView.alpha = 1
            
            UIView.animate(withDuration: 0.6, animations: {
                self.trashView.alpha = 0
                self.trashView.isHidden = true
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isTranslucent = false
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAccessoryLayerNotification), object: nil, userInfo: [ChangeAccessoryLayerNotificationShowKey:false])
        
        if let thumb = ImagesManager.sharedImagesManager.snapshot(designView){
            design?.saveThumbnail(thumb)
        }
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
        
         navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.design?.designGarments?.count  == 0{
            if !menuView.isOpened{
                menuView.open()
            }
        }
    }
    
    func addGarment(_ garment:Garment) {
        
        let view = ItemView(frame: CGRect(x: self.designView.frame.midX, y: self.designView.frame.midY, width: 160, height: 160), garment: garment, design: design!
        )
        
        let image = UIImage(contentsOfFile: garment.filePath())
        
        view.imageView.image = image
        
        self.designView.addSubview(view)
        
        view.designGarment.z =  NSNumber(value: self.designView.subviews.index(of: view)!)
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()

    }
    

    
    func loadGarmentsIfHave() {
        if let design = design,let garments = design.designGarments  , garments.count>0 {
    
            garments.sorted{($0 as AnyObject).z!!.intValue < ($1 as AnyObject).z!!.intValue}.forEach({ (object) in
                
                let dg =  object as! DesignGarment
                print(dg.garment?.objectID)
                let view = ItemView(aDesignGarment: dg )
                let image = UIImage(contentsOfFile: dg.garment!.filePath())
                view.imageView.image = image
                self.designView.addSubview(view)
            })
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if design == nil{
            let fr = NSFetchRequest<Design>(entityName: "Design")
            fr.predicate = NSPredicate(format: "designGarments.@count == 0")
            fr.fetchLimit = 1
            do {
                if let tempDesign = try CoreDataManager.sharedCoreDataManager.managedObjectContext.fetch(fr).first{
                    design = tempDesign
                    return
                }
            }catch{
                
            }
            
            design = Design.design(designView.bounds.size)
            
        }
    }
    

    @IBAction func handleTap(_ sender: AnyObject) {
        
        if let g = sender as? UITapGestureRecognizer {
            switch g.state {
            case .ended:
                NotificationCenter.default.post(name: Notification.Name(rawValue: ShouldCloseMenuNotification), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: ChangeAccessoryLayerNotification), object: nil, userInfo: [ChangeAccessoryLayerNotificationShowKey:false])
                
            default:
                break
            }
        }
    }
    

    /// Handle the menuView touch event.
    internal func handleMenu() {
        if menuView.isOpened {
            menuView.close()
            (menuView.views.first as? Button)?.animate(animation: Animation.rotate(rotation: 0))
        } else {
            menuView.open() { (v: UIView) in
                (v as? Button)?.pulse()
            }
            (menuView.views.first as? Button)?.animate(animation: Animation.rotate(rotation: 0.125))
        }
    }
    /// Handle the menuView touch event.
    @objc(handleButton:)
    internal func handleButton(_ button: UIButton) {
        if let type = GarmentType(rawValue: button.tag){
            selectedGarmentType = type
            self.performSegue(withIdentifier: "SelectionSegue", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = (segue.destination as? UINavigationController)?.topViewController as? DressSelectionCollectionViewController{
            vc.selectedGarmentType = self.selectedGarmentType
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /// General preparation statements are placed here.
    fileprivate func prepareView() {
        
        view.backgroundColor = Color.white
        
        trashView.backgroundColor = UIColor.color(240, green: 240, blue: 240, alpha: 1)
        
        trashView.image = UIImage.fontAwesomeIcon(name: .trashO, textColor: UIColor.gray, size:  CGSize(width: 100, height: 100))
    

    }
    

    /// Prepares the MenuView example.
    fileprivate func prepareMenuViewExample() {
        var image: UIImage? = UIImage(named: "ic_add_white")?.withRenderingMode(.alwaysTemplate)
        let btn1: FabButton = FabButton()
  
        btn1.tintColor = UIConfig.DefaultTintColor
        btn1.borderColor = UIConfig.DefaultTintColor
        btn1.backgroundColor = Color.white
        btn1.borderWidth = 1
        btn1.setImage(image, for: .normal)
        btn1.setImage(image, for: .highlighted)
        btn1.addTarget(self, action: #selector(handleMenu), for: .touchUpInside)
        menuView.addSubview(btn1)
        
        image = UIImage(named: "sideicon-tops-white")?.withRenderingMode(.alwaysTemplate)
        let btn2: FabButton = FabButton()
    
        btn2.tag = GarmentType.tops.rawValue
        btn2.tintColor = UIConfig.DefaultTintColor
        btn2.pulseColor = UIConfig.DefaultTintColor
        btn2.borderColor = UIConfig.DefaultTintColor
        btn2.backgroundColor = Color.white
        btn2.borderWidth = 1
        btn2.setImage(image, for: .normal)
        btn2.setImage(image, for: .highlighted)
        btn2.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        menuView.addSubview(btn2)
        
        image = UIImage(named: "sideicon-pants-white")?.withRenderingMode(.alwaysTemplate)
        let btn3: FabButton = FabButton()
     
         btn3.tag = GarmentType.pants.rawValue
        btn3.tintColor = UIConfig.DefaultTintColor
        btn3.pulseColor = UIConfig.DefaultTintColor
        btn3.borderColor = UIConfig.DefaultTintColor
        btn3.backgroundColor = Color.white
        btn3.borderWidth = 1
        btn3.setImage(image, for: .normal)
        btn3.setImage(image, for: .highlighted)
        btn3.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        menuView.addSubview(btn3)
        
        image = UIImage(named: "sideicon-shoes-white")?.withRenderingMode(.alwaysTemplate)
        let btn4: FabButton = FabButton()
   
         btn4.tag = GarmentType.shoes.rawValue
        btn4.tintColor = UIConfig.DefaultTintColor
        btn4.pulseColor = UIConfig.DefaultTintColor
        btn4.borderColor = UIConfig.DefaultTintColor
        btn4.backgroundColor = Color.white
        btn4.borderWidth = 1
        btn4.setImage(image, for: .normal)
        btn4.setImage(image, for: .highlighted)
        btn4.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        menuView.addSubview(btn4)
        
        
        image = UIImage(named: "sideicon-others-white")?.withRenderingMode(.alwaysTemplate)
        let btn5: FabButton = FabButton()
   
        btn5.tag = GarmentType.others.rawValue
        btn5.tintColor = UIConfig.DefaultTintColor
        btn5.pulseColor = UIConfig.DefaultTintColor
        btn5.borderColor = UIConfig.DefaultTintColor
        btn5.backgroundColor = Color.white
        btn5.borderWidth = 1
        btn5.setImage(image, for: .normal)
        btn5.setImage(image, for: .highlighted)
        btn5.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        menuView.addSubview(btn5)
        
        // Initialize the menu and setup the configuration options.
        menuView.direction = .up

        menuView.views = [btn1, btn2, btn3, btn4,btn5]
        
        view.addSubview(menuView)
        view.layout(menuView).width(diameter).height(diameter).bottom(16).bottomLeft()
    }



}



