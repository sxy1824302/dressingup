//
//  DateEditViewController.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class DateEditViewController: UIViewController,EditTarget {
    var applyEditing:  ((AnyObject?) -> (Void))?
    var initiateValue: AnyObject?
    @IBOutlet weak var datePicker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.locale = Locale.current
        
        guard let date = initiateValue as? Date else {
            return
        }
        
        datePicker.date = date
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func done(_ sender: AnyObject) {
        guard let closure = applyEditing else { return }
        
        closure(datePicker.date as AnyObject?)
        
        navigationController?.popViewController(animated: true)
        
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
