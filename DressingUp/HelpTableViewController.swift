//
//  HelpTableViewController.swift
//  DressingUp
//
//  Created by alexander sun on 5/30/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import MessageUI


class HelpTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()


    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath as NSIndexPath).row {
        case 0:
            
            break
        case 1:
            if (MFMailComposeViewController.canSendMail()){
                let mvc = MFMailComposeViewController()
                mvc.mailComposeDelegate = self
                mvc.setToRecipients(["contact@ijubar.cn"])
                mvc.setSubject(NSLocalizedString("Feedback", comment: ""))
                present(mvc, animated: true, completion: nil)
            }else{
                
            }
        case 2:
            var url = URLComponents(string: "itms-apps://itunes.apple.com/app/id\(1108736222)")!
            url.queryItems = [URLQueryItem(name: "onlyLatestVersion", value: "true"),URLQueryItem(name: "pageNumber", value: "0")]
            UIApplication.shared.openURL(url.url!)
        
        default:
            break
        }
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HelpTableViewController:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}

