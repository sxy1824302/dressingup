//
//  DecimalEditViewController.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class DecimalEditViewController: UIViewController,EditTarget {
    var applyEditing: ((AnyObject?) -> (Void))?
    var initiateValue: AnyObject?
    @IBOutlet weak var textField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let d = initiateValue as? Float else {
            return
        }
        
        textField.text = String(d)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func done(_ sender: AnyObject) {
        guard let t = textField.text, let value = Float(t), let closure = applyEditing else { return }
        
        closure(value as AnyObject?)
        
        navigationController?.popViewController(animated: true)
        
        
        CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
