//
//  ClosetCollectionViewController.swift
//  DressingUp
//
//  Created by alexander sun on 5/25/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import CoreData
import DZNEmptyDataSet
class ClosetCollectionViewController: UICollectionViewController {
    var fetchedResultsController:NSFetchedResultsController<Garment>!
    weak var closetViewController:ClosetViewController!
    var inEditingMode = false
    var lastPredicate:NSPredicate = NSPredicate(format: "garmentType == %lli",GarmentType.tops.rawValue)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.emptyDataSetSource = self
        collectionView?.emptyDataSetDelegate = self
        self.clearsSelectionOnViewWillAppear = true
        collectionView?.backgroundColor = UIColor.color(240, green: 240, blue: 240, alpha: 255)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadGarmentData(lastPredicate)
    }
    
    func reloadGarmentData(_ predicate:NSPredicate?) {
        let request = NSFetchRequest<Garment>(entityName: "Garment")
        let descriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        request.sortDescriptors = [descriptor]
        
        if let predicate = predicate {
            request.predicate = predicate
            lastPredicate = predicate
        }
        

        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreDataManager.sharedCoreDataManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
      
        fetchedResultsController.delegate = self
        
        do{
            try fetchedResultsController.performFetch()
            collectionView?.reloadData()
        }catch let error{
            print(error)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .vertical
            layout.sectionInset = UIEdgeInsetsMake(6, 2, 0, 2)
            layout.minimumInteritemSpacing = 1
            layout.minimumLineSpacing = 1
            
            
            let basicSide = min(view.frame.width, view.frame.height) - 2*2

            
            var smallSide:CGFloat = basicSide
            
            
            var itemPerRow:CGFloat
            
            repeat{
                itemPerRow = floor(smallSide/100)
                
                smallSide = basicSide - (itemPerRow-1)*layout.minimumInteritemSpacing
                
            }while(floor(smallSide/100) != itemPerRow)
            
            
         
            if (smallSide.truncatingRemainder(dividingBy: 100)>50){
                
                itemPerRow += 1
                
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth)
            }else{
                
    
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth)
            }
            
   
            
        }
        
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return fetchedResultsController.sections?.count ?? 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GarmentCollectionViewCell", for: indexPath) as! GarmentCollectionViewCell
 
        let dress = fetchedResultsController.object(at: indexPath)
        
        cell.configCellWithDress(dress)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dress = fetchedResultsController.object(at: indexPath)
        
        closetViewController.didSelectGarment(dress)
        
        if !inEditingMode{
            collectionView.deselectItem(at: indexPath, animated: true)
        }
    }
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let dress = fetchedResultsController.object(at: indexPath)        
        closetViewController.didDeselectGarment(dress)
    }
    
    
}
extension ClosetCollectionViewController:NSFetchedResultsControllerDelegate{
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView?.reloadData()
    }
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
    }
}

extension ClosetCollectionViewController:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string:NSLocalizedString( "Add clothes now!", comment: ""), attributes: [NSForegroundColorAttributeName:UIConfig.DefaultTintColor])
    }
    
    func buttonImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return UIImage(named: "button-add")
    }
    
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        closetViewController.showCameraFromEmpty()
    }
}
