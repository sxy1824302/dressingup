//
//  GarmentImageTableViewCell.swift
//  DressingUp
//
//  Created by alexander sun on 09/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class GarmentImageTableViewCell: UITableViewCell {
    @IBOutlet weak var garmentImageView: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor(hex6: 0xfefefe)
     
    }

 

}

class GarmentSimpleDescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    

}


class GarmentTextTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    

    
}
