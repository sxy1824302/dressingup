//
//  ImageProcessingViewController.swift
//  DressingUp
//
//  Created by alexander sun on 12/10/2016.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import GPUImage

class ImageProcessingViewController: UIViewController {
    @IBOutlet weak var thresholdSlider: UISlider!
    @IBOutlet weak var smoothingSlider: UISlider!
    @IBOutlet weak var renderView: RenderView!{
        didSet{
            renderView.backgroundRenderColor = Color(red: 1, green: 1, blue: 1)
            renderView.backgroundColor = UIColor.clear
        }
    }
    @IBOutlet weak var targetColorView: UIView!

    
    var targetImage:UIImage!
    var targetColor: Color!{
        didSet{
            targetColorView.backgroundColor = UIColor(red: CGFloat(targetColor.redComponent), green: CGFloat(targetColor.greenComponent), blue: CGFloat(targetColor.blueComponent), alpha: CGFloat(targetColor.alphaComponent))
        }
    }
    
    
    var picture:PictureInput!
    var filter = ChromaKeying()
    let output = PictureOutput()
    
    var renderedImage:UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        targetColor = Color.blue
        
        
        resetInput()
        
    }
    
    func resetInput() {
        if let pic = picture{
            pic.removeAllTargets()
        }
        
        picture = PictureInput(image:targetImage)
        
        filter.thresholdSensitivity = 0.2
        filter.colorToReplace = targetColor
        
        smoothingSlider.value = filter.smoothing
        thresholdSlider.value = filter.thresholdSensitivity
        
        process()
    }
    
    @IBAction func resetImage(_ sender: AnyObject) {
       resetInput()

    }
    @IBAction func didTapRenderView(_ sender: AnyObject) {
        let tg = sender as! UITapGestureRecognizer
        
        let location = tg.location(in: tg.view)
        
        getColor(location: location)
    }
    
    func getColor(location:CGPoint)  {
        
        print(location)
        
        UIGraphicsBeginImageContext(renderView.frame.size)
        
        let context = UIGraphicsGetCurrentContext()!
        
        UIColor.white.setFill()

        context.fill(renderView.bounds)
        
        //renderView.draw(renderView.bounds)
        self.renderedImage?.draw(in: renderView.bounds)
        

        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        let width = Int(renderView.frame.width)
        let height = Int(renderView.frame.height)
        
        guard let buffer = context.data else {
            print("unable to get context data")
            return
        }
        
  
        let offset = max((Int(location.y) - 1), 0) * width + Int(location.x)
        
        let pixelBuffer = buffer.bindMemory(to: RGBA32.self, capacity: width * height)
        
        for row in 0 ..< Int(height) {
            for column in 0 ..< Int(width) {
                let offset = row * width + column
                 print(pixelBuffer[offset])
            }
        }
      
        
        

        UIGraphicsEndImageContext()
        
        
        
    }
    
    
    @IBAction func thresholdChanged(_ sender: AnyObject) {
        filter.thresholdSensitivity = sender.value
        process()
    
    }
    @IBAction func smoothingChanged(_ sender: AnyObject) {
        filter.smoothing = sender.value
        process()
    }
    private func process() {
        output.imageAvailableCallback = { image in
            self.renderedImage = image
        }

        picture --> filter
        filter --> renderView
        filter --> output
      
        picture.processImage(synchronously: true)
    }
    


}

struct RGBA32: Equatable,CustomStringConvertible {
    var color: UInt32
    
    var red: UInt8 {
        return UInt8((color >> 24) & 255)
    }
    
    var green: UInt8 {
        return UInt8((color >> 16) & 255)
    }
    
    var blue: UInt8 {
        return UInt8((color >> 8) & 255)
    }
    
    var alpha: UInt8 {
        return UInt8((color >> 0) & 255)
    }
    
    init(red: UInt8, green: UInt8, blue: UInt8, alpha: UInt8) {
        color = (UInt32(red) << 24) | (UInt32(green) << 16) | (UInt32(blue) << 8) | (UInt32(alpha) << 0)
    }
    var description: String{
        get{
            return "\(red) \(green) \(blue) \(alpha)"
        }
    }
    
    
    static let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
    
    static func ==(lhs: RGBA32, rhs: RGBA32) -> Bool {
        return lhs.color == rhs.color
    }
}
