//
//  ImagesManager.swift
//  SmartCloset
//
//  Created by alexander sun on 4/5/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class ImagesManager {
    static let sharedImagesManager = ImagesManager()
    var imageCachePath:String
    var thumbnailCachePath:String
    init() {
        let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        
        thumbnailCachePath = docPath+"/Thumbnails"
        
        imageCachePath = docPath+"/Images"
        
        
      
        if !FileManager.default.fileExists(atPath: imageCachePath){
            do{
          
                
                try FileManager.default.createDirectory(atPath: imageCachePath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                
            }
        }
        
        if !FileManager.default.fileExists(atPath: thumbnailCachePath){
            do{
                try FileManager.default.createDirectory(atPath: thumbnailCachePath, withIntermediateDirectories: true, attributes: nil)
            }catch{
                
            }
        }
        
    }
    
    
    
    func saveImage(_ image:UIImage) -> String? {
        var path:String?
        if let data = UIImageJPEGRepresentation(image, 0.9){
            do{
                path = imageCachePath+"/\(ProcessInfo.processInfo.globallyUniqueString).jpg"
                
                try data.write(to: URL(fileURLWithPath:path!), options: .atomic)
                
                 return (path! as NSString).lastPathComponent
            }catch{
                return nil
            }
        }
       return nil
     }
    
    
    func copyBundleImageToCachePath(_ name:String) throws{
        let filePath = Bundle.main.path(forResource: name, ofType: nil, inDirectory: "sample")
        let newPath = imageCachePath+"/"+name
        try FileManager.default.copyItem(atPath: filePath!, toPath: newPath)
       
        
    }
    
    
    func snapshot(_ view:UIView)->String? {
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, true, UIScreen.main.scale)
    
        //view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        
        guard let img = image else{
            return nil
        }
        
        var path:String?
        if let data = UIImageJPEGRepresentation(img, 0.1){
            do{
                path = thumbnailCachePath+"/\(ProcessInfo.processInfo.globallyUniqueString).jpg"
                
                try data.write(to: URL(fileURLWithPath:path!), options: .atomic)
                
                return (path! as NSString).lastPathComponent
            }catch{
                return nil
            }
        }
    
        return nil
    }
    
}
