//
//  DressSelectionCollectionView.swift
//  DressingUp
//
//  Created by alexander sun on 5/24/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import CoreData

let SelectedGarmentNotification = "SelectedGarmentNotification"

class DressSelectionCollectionViewController: UICollectionViewController {
 
    var selectedGarmentType:GarmentType?
    
    var fetchedResultsController:NSFetchedResultsController<Garment>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        collectionView?.backgroundColor = UIColor.color(240, green: 240, blue: 240,alpha: 1)
        
        
    }
    
    @IBAction func cancelSelection(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let request = NSFetchRequest<Garment>(entityName: "Garment")
        let descriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        request.sortDescriptors = [descriptor]
        
        if let type = selectedGarmentType {
            request.predicate = NSPredicate(format: "garmentType == \(type.rawValue)")
        }
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreDataManager.sharedCoreDataManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do{
            try fetchedResultsController.performFetch()
        }catch let error{
            print(error)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout{
            layout.scrollDirection = .vertical
            layout.sectionInset = UIEdgeInsetsMake(6, 2, 0, 2)
            layout.minimumInteritemSpacing = 1
            layout.minimumLineSpacing = 1
            
            
            let basicSide = min(view.frame.width, view.frame.height) - 2*2
            
            
            var smallSide:CGFloat = basicSide
            
            
            var itemPerRow:CGFloat
            
            repeat{
                itemPerRow = floor(smallSide/100)
                
                smallSide = basicSide - (itemPerRow-1)*layout.minimumInteritemSpacing
                
            }while(floor(smallSide/100) != itemPerRow)
            
            
            
            if (smallSide.truncatingRemainder(dividingBy: 100)>50){
                
                itemPerRow += 1
                
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth)
            }else{
                
                
                let averageWidth = floor(smallSide/itemPerRow)
                
                layout.itemSize = CGSize(width: averageWidth, height: averageWidth)
            }
            
            
        }
        
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return fetchedResultsController.sections?.count ?? 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GarmentSelectionViewCell", for: indexPath) as! GarmentSelectionViewCell
        cell.backgroundColor = UIColor.lightGray
        
        
        let dress = fetchedResultsController.object(at: indexPath) 
        
        cell.configCellWithDress(dress)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dress = fetchedResultsController.object(at: indexPath) 
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: SelectedGarmentNotification), object: dress, userInfo: nil)
        
        dismiss(animated: true, completion: nil)
    }
    


    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
extension DressSelectionCollectionViewController:NSFetchedResultsControllerDelegate{
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView?.reloadData()
    }
}
