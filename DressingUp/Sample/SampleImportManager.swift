//
//  SampleImportManager.swift
//  SmartCloset
//
//  Created by alexander sun on 5/17/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import Foundation

class SampleImportManager {
    static let sharedSampleImportManager = SampleImportManager()
    
    let ImportKey = "ImportKey"
    
    init(){
        if let imported = UserDefaults.standard.object(forKey: ImportKey) as? Bool , imported == true{
            
            
        }else{
            
            let configs = NSArray(contentsOfFile: Bundle.main.path(forResource: "Sample", ofType: "plist")!)
            
            configs?.enumerateObjects({ (object , idx, stop) in
                if let obj = object as? Dictionary<String,AnyObject>, let name = obj["fileName"] as? String{
                    Garment.dressByMappingValues(obj)
                    
                    try! ImagesManager.sharedImagesManager.copyBundleImageToCachePath(name)
                    
                }
          
            })
            
      
            UserDefaults.standard.set(true, forKey: ImportKey)
            CoreDataManager.sharedCoreDataManager.saveIfHasChanges()
            
            print("imported \(configs?.count) samples")
        }
        
        
        
    }
    
}
