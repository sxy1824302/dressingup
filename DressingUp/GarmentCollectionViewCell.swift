//
//  GarmentCollectionViewCell.swift
//  DressingUp
//
//  Created by alexander sun on 5/19/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
import Material
class GarmentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnailView: UIImageView!
    
    @IBOutlet weak var checkIconView: UIImageView!
    @IBOutlet weak var selectionEffectView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionEffectView.isHidden = true
        checkIconView.image = Icon.check
        contentView.backgroundColor = UIColor.clear
        
        
    }
    var thumbnail:UIImage?{
        didSet{
            thumbnailView.image = thumbnail
        }
    }
    
    func configCellWithDress(_ dress:Garment) -> Void {
        
        let image = UIImage(contentsOfFile: dress.filePath())
        thumbnail = image
    
    }
    
    override var isSelected: Bool{
        didSet{
            selectionEffectView.isHidden = !isSelected
        }
    }
}


class GarmentSelectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnailView: UIImageView!
    var thumbnail:UIImage?{
        didSet{
            thumbnailView.image = thumbnail
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = UIColor.clear
    }
    
    func configCellWithDress(_ dress:Garment) -> Void {
        
        let image = UIImage(contentsOfFile: dress.filePath())
        thumbnail = image
        
        
    }
    

}

