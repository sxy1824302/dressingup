//
//  ExtendedNavBarView.swift
//  DressingUp
//
//  Created by alexander sun on 5/25/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit
class ExtendedNavBarView: UIView {
    
    override func didMoveToWindow() {
        // Use the layer shadow to draw a one pixel hairline under this view.
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0/UIScreen.main.scale)
        self.layer.shadowRadius = 0
   
        // UINavigationBar's hairline is adaptive, its properties change with
        // the contents it overlies.  You may need to experiment with these
        // values to best match your content.
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.25
       
    }
    
}
