//
//  TutorialsViewController.swift
//  DressingUp
//
//  Created by alexander sun on 6/23/16.
//  Copyright © 2016 The Arsonists. All rights reserved.
//

import UIKit

class TutorialsViewController: UIPageViewController,UIPageViewControllerDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        let iv = storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        iv.image = UIImage(named: Tutorial.sharedTutorial.images[0])
  
        self.setViewControllers([iv], direction: .forward, animated: false) { (finished) in
            
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let ivc = viewController as! ImageViewController
        if ivc.index == 0{
            return nil
        }
        let iv = storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        iv.image = UIImage(named: Tutorial.sharedTutorial.images[ivc.index-1])
        iv.index = ivc.index-1
        //print("viewControllerBeforeViewController\(ivc.index)-\(ivc.index-1)")
        return iv
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let ivc = viewController as! ImageViewController
        if ivc.index == Tutorial.sharedTutorial.images.count-1{
            return nil
        }
        
        
        let iv = storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        iv.image = UIImage(named: Tutorial.sharedTutorial.images[ivc.index+1])
        iv.index = ivc.index+1
        //print("viewControllerAfterViewController\(ivc.index)-\(ivc.index+1)")
        return iv
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return  Tutorial.sharedTutorial.images.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class ImageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    var image:UIImage!{
        didSet{
            imageView?.image = image
        }
    }
    var index  = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
    }
}

class Tutorial:NSObject {
    static let sharedTutorial = Tutorial()
    let images = ["t01","t02","t03","t04"]


}
